BlockLaunch API by BlockLaunch Team
------------------------------------

This is the WIP API for the BlockLaunch modpack
For further information use see wiki.blocklaunch.net (we are working on it) and the documentations of the classes
It is necessary to use "Minecraft Forge" (minecraftforge.net) for using and developing this modification.
	For the Version of Minecraft Forge see Changelog or build.gradle

How to use
----------------------------------
Downlaod the version of the API from build/libs
Add those as library in your environment (if you don't know how google it!)

Classes (root org.blocklaunch.api)
------------------------------------
BLapi - methods using the guis and more of BlockLaunch
BLlib - methods adding useful functions 
			>drop item, also costum delay and around Pos
			>register block/item with unlocalized name
			>register (inventory) item model of block/item with different hooks
			>check if key is element of array

Licence
-----------
BlockLaunch is a modpack for Minecraft Forge created by the BlockLaunch Team (a part of Team Melanistic)
You are able to create your own addon for BlockLaunch. It would be nice, if you post it at forum.blocklaunch.net
If you wanna use some classes in your own mod, we are happy about it but it would be nice if you mention us.
